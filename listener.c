#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <assert.h>
#include <err.h>
#include <fcntl.h>
#include <sys/stat.h>

#define DEBUG
#define BACKLOG 10
#define LOOP for(;;)

size_t readn(int fd, void *vptr, size_t n) {
    size_t nleft;
    ssize_t nread;
    char *ptr;

    ptr = vptr;
    nleft = n;
    while (nleft > 0) {
        nread = read(fd, ptr, nleft);
        if (nread < 0) {
            if (errno == EINTR) {
                nread = 0;
            } else {
                perror("readn");
                break;
            }
        } else if (nread == 0) {
            break;
        }
        nleft -= nread;
        ptr += nread;
    }
    return (n - nleft);
}

size_t writen(int fd, const void *vptr, size_t n) {
    size_t nleft;
    ssize_t nwritten;
    const char *ptr;

    ptr = vptr;
    nleft = n;
    while (nleft > 0) {
        nwritten = write(fd, ptr, nleft);
        if (nwritten <= 0) {
            if (nwritten < 0 && errno == EINTR) {
                nwritten = 0;
            } else {
                perror("writen");
                break;
            }
        }
        nleft -= nwritten;
        ptr += nwritten;
    }
    return n - nleft;
}

int rmws(char *str) {
    if (!str)
        return -1;
    char buf[strlen(str) + 1];
    memset(buf, 0, sizeof(buf));
    char *i = str;
    while (isspace(*i))
        ++i;
    if (*i == 0) {
        str = 0;
        return 0;
    }
    char *j = i;
    while (!isspace(*j))
        ++j;
    memcpy(buf, i, j - i + 1);
    memcpy(str, buf, j - i + 1);
    str[j - i] = 0;
    return 0;
}

int tcp_listener(const char *port) {
    if (!port) {
        fprintf(stderr, "tcp_listener: port unspecified\n");
        return -1;
    }

    int server_fd;
    struct addrinfo hints, *res0, *res;
    int yes = 1;
    int error;
    // store Tcp Listener socket info
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    // TODO Change to use ip address passed in
    hints.ai_flags = AI_PASSIVE;
    error = getaddrinfo(NULL, port, &hints, &res0);
    if (error) {
        fprintf(stderr, "tcp_listener: %s\n", gai_strerror(error));
        return -1;
    }
    for (res = res0; res != NULL; res = res->ai_next) {
        server_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (server_fd == -1) {
            perror("tcp_listener: socket");
            continue;
        }
        error = setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
        if (error) {
            perror("tcp_listener: setsockopt");
            continue;
        }
        error = bind(server_fd, res->ai_addr, res->ai_addrlen);
        if (error) {
            close(server_fd);
            perror("tcp_listener: bind");
            continue;
        }
        break;
    }
    freeaddrinfo(res0);
    if (res == NULL) {
        return -1;
    }
    error = listen(server_fd, BACKLOG);
    if (error) {
        perror("tcp_listener: listen");
        return -1;
    }
    return server_fd;
}

void handle_client(int client_fd) {
    ssize_t n;
    const char *errmsg;
    FILE *fp = NULL;
    size_t fsize;
    char *fbuf = NULL;
    char fname[256];

    LOOP {
        memset(fname, 0, sizeof(fname));
        n = recv(client_fd, fname, 255, 0);
        if (n == 0) {
            fprintf(stdout, "Client disconnected, going to cleanup.\n");
            goto cleanup0;
        } else if (n < 0) {
            perror("recv");
            fprintf(stderr, "Going to cleanup.\n");
            goto cleanup0;
        }

        rmws(fname);
        fp = fopen(fname, "rb");
        if (!fp) {
            perror("fopen");
            errmsg = strerror(errno);
            n = writen(client_fd, errmsg, strlen(errmsg));
            if (n != strlen(errmsg)) {
                perror("writen(fopen_errmsg)");
            }
            goto cleanup1;
        }
        fseek(fp, 0, SEEK_END);
        fsize = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        fbuf = malloc(fsize);
        n = fread(fbuf, fsize, 1, fp);
        if (n != 1) {
            perror("fread");
            errmsg = strerror(errno);
            n = writen(client_fd, errmsg, strlen(errmsg));
#ifdef DEBUG
            if (n < strlen(errmsg)) {
                fprintf(stderr, "Error writing to "
                        "socket: %lu/%lu bytes written.\n",
                        n, strlen(errmsg));
                perror("writen");
            }
#endif
            goto cleanup2;
        }
        n = writen(client_fd, fbuf, fsize);
        if (n < fsize) {
            fprintf(stderr, "Error writing to "
                    "socket: %lu/%lu bytes written.\n",
                    n, strlen(errmsg));
            perror("writen");
        }
cleanup2:
        free(fbuf);
cleanup1:
        fclose(fp);
    }

cleanup0:
    close(client_fd);
    exit(0);
}

int main(int argc, char *argv[]) {
    int error;
    int index;
    int server_fd;
    int client_fd;
    int c;

    if (argc < 2) {
        fprintf(stderr, "Requires port number argument.\n");
        exit(1);
    }
    opterr = 0;
    while ((c = getopt(argc, argv, "abc:")) != -1) {
        switch (c) {
        case '?':
            if (isprint(optopt)) {
                fprintf(stderr, "Unknown option '-%c'.\n", optopt);
            } else {
                fprintf(stderr, "Unknown option character '%x'.\n", optopt);
            }
            exit(1);
        }
    }
    fprintf(stdout, "Attempting to get TCP listener...\n");
    server_fd = tcp_listener(argv[1]);
    if (server_fd == -1) {
        fprintf(stdout, "Failed to retrieve TCP listener, exiting...\n");
        exit(1);
    } else {
        fprintf(stdout, "Retrieved TCP listener, socket #%d, "
                "accepting connections...\n", server_fd);
    }
    LOOP {
        client_fd = accept(server_fd, 0, 0);
        if (client_fd == -1) {
            perror("accept");
            continue;
        }
        if (!fork()) {
            close(server_fd);
            handle_client(client_fd);
        } else {
            close(client_fd);
        }
    }
    close(server_fd);
}
